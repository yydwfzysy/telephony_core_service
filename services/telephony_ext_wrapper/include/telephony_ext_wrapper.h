/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TELEPHONY_EXT_WRAPPER_H
#define TELEPHONY_EXT_WRAPPER_H

#include "nocopyable.h"
#include "singleton.h"
#include "network_state.h"
#include "network_search_types.h"
#include "network_search_result.h"
#include "signal_information.h"
#include "network_state.h"
#include "cell_information.h"

namespace OHOS {
namespace Telephony {
class TelephonyExtWrapper final {
DECLARE_DELAYED_REF_SINGLETON(TelephonyExtWrapper);

public:
    DISALLOW_COPY_AND_MOVE(TelephonyExtWrapper);
    void InitTelephonyExtWrapper();

    typedef bool (*CHECK_OPC_VERSION_IS_UPDATE)(void);
    typedef void (*UPDATE_OPC_VERSION)(void);
    typedef char* (*GET_VOICE_MAIL_ICCID_PARAMETER)(int32_t, const char*);
    typedef void (*SET_VOICE_MAIL_ICCID_PARAMETER)(int32_t, const char*, const char*);
    typedef void (*INIT_VOICE_MAIL_MANAGER_EXT)(int32_t);
    typedef void (*DEINIT_VOICE_MAIL_MANAGER_EXT)(int32_t);
    typedef void (*RESET_VOICE_MAIL_LOADED_FLAG_EXT)(int32_t);
    typedef void (*SET_VOICE_MAIL_ON_SIM_EXT)(int32_t, const char*, const char*);
    typedef bool (*GET_VOICE_MAIL_FIXED_EXT)(int32_t, const char*);
    typedef char* (*GET_VOICE_MAIL_NUMBER_EXT)(int32_t, const char*);
    typedef char* (*GET_VOICE_MAIL_TAG_EXT)(int32_t, const char*);
    typedef void (*RESET_VOICE_MAIL_MANAGER_EXT)(int32_t);
    typedef void (*GET_NETWORK_STATUS_EXT)(int32_t slotId, sptr<OHOS::Telephony::NetworkState> &networkState);
	
    typedef int32_t (*GET_CELL_INFO_LIST)(int32_t slotId, std::vector<sptr<OHOS::Telephony::CellInformation>> &cells);
    typedef void (*GET_RADIO_TECH_EXT)(int32_t slotId, int32_t &domainRadioTech);
    typedef void (*GET_NR_OPTION_MODE_EXT)(int32_t slotId, int32_t &mode);
    typedef void (*GET_NR_OPTION_MODE_EXTEND)(int32_t slotId, OHOS::Telephony::NrMode &mode);
    typedef void (*GET_PREFERRED_NETWORK_EXT)(int32_t &preferredNetworkType);
    typedef bool (*IS_NR_SUPPORTED_NATIVE)(int32_t modemRaf);
    typedef void (*GET_SIGNAL_INFO_LIST_EXT)(int32_t slotId,
	    std::vector<sptr<OHOS::Telephony::SignalInformation>> &signals);
    typedef void (*GET_NETWORK_CAPABILITY_EXT)(int32_t slotId, int32_t networkCapabilityType,
	    int32_t &networkCapabilityState);
    typedef void (*ON_GET_NETWORK_SEARCH_INFORMATION_EXT)(int32_t &availableSize,
        std::vector<OHOS::Telephony::NetworkInformation> &networkInformations);

    typedef void (*UPDATE_COUNTRY_CODE_EXT)(int32_t, const char *);
    typedef void (*UPDATE_TIME_ZONE_OFFSET_EXT)(int32_t, int32_t);

    CHECK_OPC_VERSION_IS_UPDATE checkOpcVersionIsUpdate_ = nullptr;
    UPDATE_OPC_VERSION updateOpcVersion_ = nullptr;
    GET_VOICE_MAIL_ICCID_PARAMETER getVoiceMailIccidParameter_ = nullptr;
    SET_VOICE_MAIL_ICCID_PARAMETER setVoiceMailIccidParameter_ = nullptr;
    INIT_VOICE_MAIL_MANAGER_EXT initVoiceMailManagerExt_ = nullptr;
    DEINIT_VOICE_MAIL_MANAGER_EXT deinitVoiceMailManagerExt_ = nullptr;
    RESET_VOICE_MAIL_LOADED_FLAG_EXT resetVoiceMailLoadedFlagExt_ = nullptr;
    SET_VOICE_MAIL_ON_SIM_EXT setVoiceMailOnSimExt_ = nullptr;
    GET_VOICE_MAIL_FIXED_EXT getVoiceMailFixedExt_ = nullptr;
    GET_VOICE_MAIL_NUMBER_EXT getVoiceMailNumberExt_ = nullptr;
    GET_VOICE_MAIL_TAG_EXT getVoiceMailTagExt_ = nullptr;
    RESET_VOICE_MAIL_MANAGER_EXT resetVoiceMailManagerExt_ = nullptr;
    GET_NETWORK_STATUS_EXT getNetworkStatusExt_ = nullptr;

    GET_CELL_INFO_LIST getCellInfoList_ = nullptr;
    GET_RADIO_TECH_EXT getRadioTechExt_ = nullptr;
    GET_NR_OPTION_MODE_EXT getNrOptionModeExt_ = nullptr;
    GET_NR_OPTION_MODE_EXTEND getNrOptionModeExtend_ = nullptr;
    GET_PREFERRED_NETWORK_EXT getPreferredNetworkExt_ = nullptr;
    IS_NR_SUPPORTED_NATIVE isNrSupportedNative_ = nullptr;
    GET_SIGNAL_INFO_LIST_EXT getSignalInfoListExt_ = nullptr;
    GET_NETWORK_CAPABILITY_EXT getNetworkCapabilityExt_ = nullptr;
    ON_GET_NETWORK_SEARCH_INFORMATION_EXT onGetNetworkSearchInformationExt_ = nullptr;

    UPDATE_COUNTRY_CODE_EXT updateCountryCodeExt_ = nullptr;
    UPDATE_TIME_ZONE_OFFSET_EXT updateTimeZoneOffsetExt_ = nullptr;

private:
    void* telephonyExtWrapperHandle_ = nullptr;
    void InitTelephonyExtWrapperForNetWork();
    void InitTelephonyExtWrapperForVoiceMail();
};

#define TELEPHONY_EXT_WRAPPER ::OHOS::DelayedRefSingleton<TelephonyExtWrapper>::GetInstance()
} // namespace Telephony
} // namespace OHOS
#endif // TELEPHONY_EXT_WRAPPER_H
